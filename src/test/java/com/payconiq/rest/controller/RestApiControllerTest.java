package com.payconiq.rest.controller;

import com.payconiq.rest.Configuration.CustomHttpHeaders;
import com.payconiq.rest.Configuration.CustomParameterizedTypeReference;
import com.payconiq.rest.Configuration.RestTemplateGenerator;
import com.payconiq.rest.RestApiApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;

import java.text.MessageFormat;
import java.util.Collection;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class RestApiControllerTest extends AbstractRestApiService{

    @Autowired
    private CustomHttpHeaders customHttpHeaders;

    @Autowired
    private CustomParameterizedTypeReference customParameterizedTypeReference;

    @Autowired
    private RestTemplateGenerator restTemplateGenerator;

    @Test
    public void getAutAccessToken() {
        try {
            OAuth2AccessToken oAuth2AccessToken = getOAuthToken();
            Assert.assertNotEquals(oAuth2AccessToken, null);

        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getAllStocks() {
        try {
            MessageFormat messageFormat = new MessageFormat("http://localhost:8080/api/stocks");
            String url = messageFormat.toPattern();
            OAuth2AccessToken oAuth2AccessToken = getOAuthToken();
            Assert.assertNotEquals(oAuth2AccessToken, null);
            HttpEntity<HttpHeaders> header = customHttpHeaders.customHttpHeadersHttpEntity(oAuth2AccessToken);

            OAuth2RestTemplate oAuth2RestTemplate = oAuth2RestTemplate();
            ResponseEntity<Collection<Stock>> list = oAuth2RestTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    header,
                    customParameterizedTypeReference.parameterizedTypeCollectionOfStocks());

            Assert.assertEquals(list.getStatusCode(), HttpStatus.OK);
            Assert.assertNotEquals(list.getBody(), null);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getStock() {
        try {
            MessageFormat messageFormat = new MessageFormat("http://localhost:8080/api/stocks/1");
            String url = messageFormat.toPattern();
            OAuth2AccessToken oAuth2AccessToken = getOAuthToken();
            Assert.assertNotEquals(oAuth2AccessToken, null);

            HttpEntity<HttpHeaders> header = customHttpHeaders.customHttpHeadersHttpEntity(oAuth2AccessToken);
            OAuth2RestTemplate oAuth2RestTemplate = oAuth2RestTemplate();
            ResponseEntity<Stock> stock = oAuth2RestTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    header,
                    customParameterizedTypeReference.parameterizedTypeCollectionOfOneStock());

            Assert.assertEquals(stock.getStatusCode(), HttpStatus.OK);
            Assert.assertNotEquals(stock.getBody(), null);

        } catch (RestClientException e) {

        }
    }

    @Test
    public void updateStock() {
        try {
            MessageFormat messageFormat = new MessageFormat("http://localhost:8080/api/stocks/1");
            String url = messageFormat.toPattern();
            OAuth2AccessToken oAuth2AccessToken = getOAuthToken();
            Assert.assertNotEquals(oAuth2AccessToken, null);

            HttpEntity<HttpHeaders> header = customHttpHeaders.customHttpHeadersHttpEntity(oAuth2AccessToken);

            OAuth2RestTemplate oAuth2RestTemplate = oAuth2RestTemplate();
            Integer currentPrice = 999;
            HttpEntity<Integer> requestEntity = new HttpEntity<>(currentPrice, header.getHeaders());

            ResponseEntity<Stock> stock = oAuth2RestTemplate.exchange(
                    url,
                    HttpMethod.PUT,
                    requestEntity,
                    customParameterizedTypeReference.parameterizedTypeCollectionOfOneStock());

            Assert.assertEquals(stock.getStatusCode(), HttpStatus.OK);
            Assert.assertNotEquals(stock.getBody(), null);


        } catch (RestClientException e) {

        }
    }

    @Test
    public void insertStock() {
        try {
            MessageFormat messageFormat = new MessageFormat("http://localhost:8080/api/stocks/1");
            String url = messageFormat.toPattern();
            OAuth2AccessToken oAuth2AccessToken = getOAuthToken();
            Assert.assertNotEquals(oAuth2AccessToken, null);

            HttpEntity<HttpHeaders> header = customHttpHeaders.customHttpHeadersHttpEntity(oAuth2AccessToken);

            OAuth2RestTemplate oAuth2RestTemplate = oAuth2RestTemplate();
            Stock stock = new Stock(100, "Stock100", 999);
            stock.setCurrentPrice(999);

            HttpEntity<Stock> requestEntity = new HttpEntity<>(stock, header.getHeaders());

            ResponseEntity<Stock> stockReturn = oAuth2RestTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    requestEntity,
                    customParameterizedTypeReference.parameterizedTypeCollectionOfOneStock());

            Assert.assertEquals(stockReturn.getStatusCode(), HttpStatus.OK);
            Assert.assertNotEquals(stockReturn.getBody(), null);

        } catch (RestClientException e) {

        }
    }

    @Test
    public void getServerMetric() {
        MessageFormat messageFormat = new MessageFormat("http://localhost:8080/statistics");
        String url = messageFormat.format(new Object[]{});

        OAuth2AccessToken oAuth2AccessToken = getOAuthToken();
        Assert.assertNotEquals(oAuth2AccessToken, null);

        HttpEntity<HttpHeaders> header = customHttpHeaders.customHttpHeadersHttpEntity(oAuth2AccessToken);
        OAuth2RestTemplate oAuth2RestTemplate = oAuth2RestTemplate();
        ResponseEntity<ServerMetrics> list = oAuth2RestTemplate.exchange(
                url,
                HttpMethod.GET,
                header,
                customParameterizedTypeReference.parameterizedTypeServerMetrics());

        Assert.assertEquals(list.getStatusCode(), HttpStatus.OK);
        Assert.assertNotEquals(list.getBody(), null);
    }


}
