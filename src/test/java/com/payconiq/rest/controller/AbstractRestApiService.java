package com.payconiq.rest.controller;

import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.util.Arrays;

/**
 * Created by inano on 8.01.2017.
 */
public abstract class AbstractRestApiService {

    public AbstractRestApiService(){

    }

    protected OAuth2AccessToken getOAuthToken() {
        OAuth2RestTemplate restTemplate = null;

        try {
            ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
            resource.setAccessTokenUri("http://localhost:8080/oauth/token");
            resource.setClientId("rest-api-client");
            resource.setId("stocks-api");
            resource.setClientSecret("psw");
            resource.setScope(Arrays.asList("read"));

            DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
            restTemplate = new OAuth2RestTemplate(resource, clientContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return restTemplate.getAccessToken();
    }

    protected OAuth2RestTemplate oAuth2RestTemplate() {
        OAuth2RestTemplate restTemplate = new OAuthAbstractSecurityConfigurationBuilder()
                .setTokenUrl("http://localhost:8080/oauth/token")
                .setClientId("rest-api-client")
                .setClientSecret("psw")
                .setId("stocks-api")
                .setScope(new String[]{"read","write","trust"})
                .build();
        return restTemplate;
    }

    protected class OAuthAbstractSecurityConfigurationBuilder{
        private String tokenUrl;
        private String clientId;
        private String id;
        private String clientSecret;
        private String[] scope;

        public String getTokenUrl() {
            return tokenUrl;
        }

        public OAuthAbstractSecurityConfigurationBuilder setTokenUrl(String tokenUrl) {
            this.tokenUrl = tokenUrl;
            return this;
        }

        public String getClientId() {
            return clientId;
        }

        public OAuthAbstractSecurityConfigurationBuilder setClientId(String clientId) {
            this.clientId = clientId;
            return this;
        }

        public String getId() {
            return id;
        }

        public OAuthAbstractSecurityConfigurationBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public String getClientSecret() {
            return clientSecret;
        }

        public OAuthAbstractSecurityConfigurationBuilder setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
            return this;
        }

        public String[] getScope() {
            return scope;
        }

        public OAuthAbstractSecurityConfigurationBuilder setScope(String[] scope) {
            this.scope = scope;
            return this;
        }


        public OAuth2RestTemplate build(){

            return getInstance(this);
        }

        private OAuth2RestTemplate getInstance(OAuthAbstractSecurityConfigurationBuilder builder){
            ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
            resource.setAccessTokenUri(builder.tokenUrl);
            resource.setClientId(builder.clientId);
            resource.setId(builder.id);
            resource.setClientSecret(builder.clientSecret);
            resource.setScope(Arrays.asList(builder.scope));

            OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, new DefaultOAuth2ClientContext());

            return restTemplate;
        }
    }
}

