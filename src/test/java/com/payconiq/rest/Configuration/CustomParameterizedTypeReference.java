package com.payconiq.rest.Configuration;

import com.payconiq.rest.model.ServerMetrics;
import com.payconiq.rest.model.Stock;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Created by inano on 13.03.2018.
 */

@Component
public class CustomParameterizedTypeReference {

    public <T> ParameterizedTypeReference<Collection<Stock>> parameterizedTypeCollectionOfStocks(){
        ParameterizedTypeReference<Collection<Stock>> parameterizedTypeReference =
                new ParameterizedTypeReference<Collection<Stock>>(){};
        return parameterizedTypeReference;
    }

    public <T> ParameterizedTypeReference<Stock> parameterizedTypeCollectionOfOneStock(){
        ParameterizedTypeReference<Stock> parameterizedTypeReference =
                new ParameterizedTypeReference<Stock>(){};
        return parameterizedTypeReference;
    }

    public <T> ParameterizedTypeReference<ServerMetrics> parameterizedTypeServerMetrics(){
        ParameterizedTypeReference<ServerMetrics> parameterizedTypeReference =
                new ParameterizedTypeReference<ServerMetrics>(){};
        return parameterizedTypeReference;
    }
}

