package com.payconiq.rest.service;

import com.payconiq.rest.RestApiApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collection;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RestApiApplication.class)
public class StockServiceTest {

    private int stockIdToChange = 1;
    private int stockPriceToChange = 999;
    private int stockIdToInsert = 100;
    private String stockNameToChange = "Stock100";


    @Autowired
    private StockService stockService;

    @Test
    public void getAllStocks() {
        Collection<Stock> stockCollection = stockService.getAllStocks();
        Assert.assertNotEquals(stockCollection, null);
    }

    @Test
    public void getStockWithId() {
        Stock stock = stockService.getStockWithId(stockIdToChange);
        Assert.assertNotEquals(stock, null);
    }

    @Test
    public void updateStockPrice() {
        Stock stock = stockService.updateStockPrice(stockIdToChange, stockPriceToChange);

        Assert.assertNotEquals(stock, null);
        Assert.assertNotEquals(java.util.Optional.ofNullable(stock.getLastUpdate()), null);
        Assert.assertEquals(java.util.Optional.ofNullable(stock.getCurrentPrice()), stockPriceToChange);
    }

    @Test
    public void createStock() {
        Stock stock = new Stock(stockIdToInsert, stockNameToChange, stockPriceToChange);
        stock.setCurrentPrice(stockPriceToChange);

        Stock stockInserted = stockService.createStock(stock);
        Assert.assertNotEquals(stockInserted, null);
        Assert.assertEquals(stockInserted, stock);
    }
}
