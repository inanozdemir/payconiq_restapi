package com.payconiq.rest.statistic;

import com.google.common.util.concurrent.AtomicDouble;
import com.payconiq.rest.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletComponentScan;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import java.util.Objects;

/**
 * Created by inano on 8.03.2018.
 */
@WebListener
@ServletComponentScan
public class RequestListener implements ServletRequestListener {
    @Autowired
    MetricService metricService;

    @Override
    public void requestDestroyed(ServletRequestEvent event) {
        if(Objects.nonNull(event.getServletRequest().getAttribute("executeTime"))) {
            String executeTime = event.getServletRequest().getAttribute("executeTime").toString();
            metricService.processExecutionTime(new AtomicDouble(new Double(executeTime)));
        }
    }

    @Override
    public void requestInitialized(ServletRequestEvent event) {

    }

}
