package com.payconiq.rest.Util;

import com.payconiq.rest.model.Stock;
import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletResponse;

public class ResourceCreated extends ApplicationEvent{

    private HttpServletResponse response;
    private Stock stock;

    public ResourceCreated(Object source,
                           HttpServletResponse response, Stock stock) {
        super(source);

        this.response = response;
        this.stock = stock;
    }

    public HttpServletResponse getResponse() {
        return response;
    }
    public Stock getStock() {
        return stock;
    }
}
