package com.payconiq.rest.service;

import com.payconiq.rest.model.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service
public class StockService {


    private static final Logger logger = LoggerFactory.getLogger(StockService.class);
    public Map<Integer, Stock> stockList = new HashMap<>();

    public StockService (){
        stockList.put(1, new Stock(1, "Stock1", 10));
        stockList.put(2, new Stock(2, "Stock2", 20));
        stockList.put(3, new Stock(3, "Stock3", 30));
        stockList.put(4, new Stock(4, "Stock4", 40));
        stockList.put(5, new Stock(5, "Stock5", 50));
        stockList.put(6, new Stock(6, "Stock6", 60));
        stockList.put(7, new Stock(7, "Stock7", 70));
        stockList.put(8, new Stock(8, "Stock8", 80));
        stockList.put(9, new Stock(9, "Stock9", 90));
        stockList.put(10, new Stock(10, "Stock10", 100));
        stockList.put(11, new Stock(11, "Stock11", 110));
        stockList.put(12, new Stock(12, "Stock12", 120));
        stockList.put(13, new Stock(13, "Stock13", 130));
        stockList.put(14, new Stock(14, "Stock14", 140));
        stockList.put(15, new Stock(15, "Stock15", 150));
        stockList.put(16, new Stock(16, "Stock16", 160));
        stockList.put(17, new Stock(17, "Stock17", 170));
        stockList.put(18, new Stock(18, "Stock18", 180));
        stockList.put(19, new Stock(19, "Stock19", 190));
        stockList.put(20, new Stock(20, "Stock20", 200));
    }


    public Collection<Stock> getAllStocks() {
        return stockList.values();
    }

    public Stock getStockWithId(int id) {
        return stockList.containsKey(id) ? stockList.get(id) : null;
    }

    public Stock updateStockPrice(int id, Integer price) {
        Stock stock = null;
        try {
            if(stockList.containsKey(id)) {
               stock = stockList.get(id);
               stock.setCurrentPrice(price);
               stock.setLastUpdate(new Timestamp(System.currentTimeMillis()));
               stockList.put(id, stock);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stock;
    }

    public Stock createStock(Stock stock) {
        try {
            stock.setLastUpdate(new Timestamp(System.currentTimeMillis()));
            stockList.put(stock.getId(), stock);
        } catch (Exception e) {
            stock = null;
        }
        return stock;
    }
}
