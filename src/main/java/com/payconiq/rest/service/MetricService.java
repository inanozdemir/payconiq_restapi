package com.payconiq.rest.service;

import com.google.common.util.concurrent.AtomicDouble;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by inano on 8.03.2018.
 */
@Service
@Scope("singleton")
public class MetricService {

    private ConcurrentMap<String, Object> statusMetric;
    private AtomicInteger numberOfTotalRequests = new AtomicInteger(0);
    private AtomicDouble avgTimeOfAllRequests = new AtomicDouble(0);
    private AtomicDouble minTimOfAllRequests = new AtomicDouble(Double.MAX_VALUE);
    private AtomicDouble maxTimOfAllRequests = new AtomicDouble(0);

    public MetricService() {
        statusMetric = new ConcurrentHashMap<String, Object>();
    }

    public void increaseStatusMetricCount(String request, String status) {
        AtomicInteger statusCount = new AtomicInteger(0);
        status = "status" + status.substring(0,1) + "xx";
        Object mapValue = statusMetric.get(status);
        if(mapValue != null) {
            statusCount = new AtomicInteger(NumberUtils.createInteger(statusMetric.get(status).toString()));
        }

        if (Objects.equals(statusCount, 0)) {
            statusMetric.put(status, 1);
        } else {
            statusMetric.put("numberOfTotalRequests", numberOfTotalRequests.getAndIncrement());
            statusCount.getAndIncrement();
            statusMetric.put(status , statusCount);
        }
    }

    public Map getStatusMetric() {
        return statusMetric;
    }

    public void processExecutionTime(AtomicDouble time){
        setMaxTime(time);
        setMinTime(time);
        calculateAvgTime(time);
    }

    private void setMaxTime(AtomicDouble time){
        if(time.get() > maxTimOfAllRequests.get()) {
            maxTimOfAllRequests = time;
            statusMetric.put("maxTimOfAllRequests", maxTimOfAllRequests);
        }

    }

    private void setMinTime(AtomicDouble time){
        if(time.get() < minTimOfAllRequests.get()) {
            minTimOfAllRequests = time;
            statusMetric.put("minTimOfAllRequests", minTimOfAllRequests);
        }
    }

    private void calculateAvgTime(AtomicDouble time){
        AtomicDouble total = new AtomicDouble(avgTimeOfAllRequests.get() * numberOfTotalRequests.get());
        avgTimeOfAllRequests = new AtomicDouble(total.get() + time.get() / (numberOfTotalRequests.get()));
        statusMetric.put("avgTimeOfAllRequests", avgTimeOfAllRequests);
    }

    public AtomicInteger getNumberOfTotalRequests() {
        return numberOfTotalRequests;
    }

    public AtomicDouble getAvgTimeOfAllRequests() {
        return avgTimeOfAllRequests;
    }

    public AtomicDouble getMinTimOfAllRequests() {
        return minTimOfAllRequests;
    }

    public AtomicDouble getMaxTimOfAllRequests() {
        return maxTimOfAllRequests;
    }


}
