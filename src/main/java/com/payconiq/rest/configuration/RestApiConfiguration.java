package com.payconiq.rest.configuration;

import com.payconiq.rest.statistic.ExecuteTimeInterceptor;
import com.payconiq.rest.statistic.MetricFilter;
import com.payconiq.rest.statistic.RequestListener;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by inano on 8.01.2017.
 */

@Configuration
@ComponentScan
@EnableWebMvc
public class RestApiConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public RequestListener executorListener() {
        return new RequestListener();
    }

    @Bean
    public FilterRegistrationBean someFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new MetricFilter());
        registration.addUrlPatterns("/*");
        registration.setName("metricFilter");
        registration.setOrder(1);
        return registration;
    }

    @Bean(name = "metricFilter")
    public MetricFilter someFilter() {
        return new MetricFilter();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ExecuteTimeInterceptor());
    }
}
