package com.payconiq.rest.controller;

import com.google.common.base.Preconditions;
import com.payconiq.rest.model.Stock;
import com.payconiq.rest.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.Callable;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/api")
public class RestApiController {

    @Autowired
    private StockService stockService;

    @RequestMapping(value = "/stocks", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Callable<ResponseEntity> getAllStocks() {
        Collection<Stock> collection = stockService.getAllStocks();
        return () -> collection.size() > 0 ? new ResponseEntity<>(stockService.getAllStocks(), HttpStatus.OK) : new ResponseEntity<>("No Data Found", HttpStatus.OK);
    }

    @RequestMapping(value = "/stocks/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Callable<ResponseEntity> getStockWithId(@PathVariable("id") int id) {
        Stock stock = stockService.getStockWithId(id);
        return () -> Objects.nonNull(stock) ? new ResponseEntity<>(stock, HttpStatus.OK) : new ResponseEntity<>("No Data Found With id :" + id, HttpStatus.OK);
    }

    @RequestMapping(value = "/stocks/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Callable<ResponseEntity> updateExist(@RequestBody Stock stock1, @PathVariable("id")  int id)  {
        Preconditions.checkNotNull(stock1.getCurrentPrice());
        Stock stock = stockService.updateStockPrice(id, stock1.getCurrentPrice());
        return () -> Objects.nonNull(stock) ? new ResponseEntity<>(stock, HttpStatus.OK) : new ResponseEntity<>("No Data Found With id :" + id, HttpStatus.OK);
    }

    @RequestMapping(value = "/stocks", method = RequestMethod.POST)
    public Callable<ResponseEntity> addToStock(@RequestBody Stock stock) {
        Preconditions.checkNotNull(stock);
        stockService.createStock(stock);
        return () -> Objects.nonNull(stock) ? new ResponseEntity<>(stock, HttpStatus.OK) : new ResponseEntity<>("No Data Inserted", HttpStatus.OK);
    }
}


