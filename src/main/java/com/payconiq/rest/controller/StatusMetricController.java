package com.payconiq.rest.controller;

import com.payconiq.rest.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by inano on 8.01.2017.
 */
@RestController
public class StatusMetricController {

    @Autowired
    MetricService metricService;

    @RequestMapping(value = "/statistics", method = GET)
    public Map getStatusMetric() {
        return metricService.getStatusMetric();
    }
}
